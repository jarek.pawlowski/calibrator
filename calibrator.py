import math

import numpy as np
import matplotlib.pyplot as plt

import PySpice.Logging.Logging as Logging
logger = Logging.setup_logging()
from PySpice.Plot.BodeDiagram import bode_diagram
from PySpice.Spice.Netlist import Circuit
from PySpice.Unit import *

circuit = Circuit('calibration multi LC circuit')

L1 = 1@u_uH
C1 = 1@u_nF

L2 = 1@u_uH
C2 = 10@u_nF

L3 = 10@u_uH
C3 = 10@u_nF

L4 = 10@u_uH
C4 = 100@u_nF

L5 = 100@u_uH
C5 = 1000@u_nF

circuit.SinusoidalVoltageSource('input', 'in', circuit.gnd, amplitude=1@u_V)

circuit.R(1, 'in',   'out0', 1@u_mΩ)
circuit.L(1, 'out0', 'out1' , L1)
circuit.C(1, 'out1', circuit.gnd, C1)

circuit.L(2, 'out1', 'out2' , L2)
circuit.C(2, 'out2', circuit.gnd, C2)

circuit.L(3, 'out2', 'out3' , L3)
circuit.C(3, 'out3', circuit.gnd, C3)

circuit.L(4, 'out3', 'out4' , L4)
circuit.C(4, 'out4', circuit.gnd, C4)

circuit.L(5, 'out4', 'out5' , L5)
circuit.C(5, 'out5', circuit.gnd, C5)


res_freq_1 = 1./(2.*np.pi*math.sqrt(L1*C1))
print(res_freq_1)

simulator = circuit.simulator(temperature=25, nominal_temperature=25)
analysis = simulator.ac(start_frequency=1@u_kHz, stop_frequency=30@u_MHz, number_of_points=100,  variation='dec')

fig = plt.figure(1, (20, 10))
plt.title("Bode Diagrams of RLC Filters")
axes = (plt.subplot(211), plt.subplot(212))

#outs = ['out1','out2','out3','out4','out5']
outs = ['out1']

for out in outs:
    bode_diagram(axes=axes,
                 frequency=analysis.frequency,
                 gain=20*np.log10(np.absolute(analysis[out])),
                 phase=np.angle(analysis[out], deg=False),
                 marker='.',
                 color='blue',
                 linestyle='-',)
for axe in axes:
    axe.axvline(x=res_freq_1, color='red')

#plt.tight_layout()
plt.show()

fig.savefig('bode-diagram.pdf',format='pdf',bbox_inches='tight')